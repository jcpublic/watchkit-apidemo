//
//  ViewController.swift
//  SunriseAPIDemo
//
//  Created by MacStudent on 2019-10-16.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    @IBOutlet weak var resultsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickButtonPressed(_ sender: Any) {
        print("Button pressed")
        
        // 1. Go to sunrise-sunset api
        // & wait for the website to repond
    AF.request("https://api.sunrise-sunset.org/json?lat=43.6532&lng=-79.3832").responseJSON {
        
            (xyz) in
            print(xyz.value)
        
            // convert the response to a JSON object
            let x = JSON(xyz.value)
            let sunrise = x["results"]["sunrise"]
            let sunset = x["results"]["sunset"]
        
            print("Sunrise: \(sunrise)")
            print("Sunset: \(sunset)")
            self.resultsLabel.text = "Sunrise: \(sunrise)"
        }
       
    }
}

